# Utilisation des volumes et des secrets Kubernetes



## Liste des fichiers utilisés sur ce dépôt
Le dépôt comporte 4 répertoires dans lesquels figurent les fichiers YAML utilisés dans la formation :
- ConfigMap : myconfigmap.yaml et mypod.yaml
- PersistentVolume: mypod.yaml, mypv.yaml et mypvc.yaml
- Secret : mypod.yaml et mysecret.yaml
- Volume Emptydir : pod-emptydir.yaml
